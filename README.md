
[![Swift 5](https://img.shields.io/badge/Swift-5-green.svg?style=flat)](https://swift.org/)

Xcode Version 12.2 (12B45b) 

# Daily Forecast Application - iOS Coding Challenge for Orcas #

Daily forecast application is a simple application consisting of only one screen with a top bar containing a text field accepting the city name then when the user click search app should hit api and get daily forecast data for given city name, and cache it.
Each day in days list should contain any ui item (it’s up to you) to indicate weather description like (​Rain, Snow, Extreme etc.).
If a user faces any failure in data retrieving app should first check if needed data exists in local cache if yes app should display cached data and show some warning to indicate it’s not accurate data, if no data cached he should see UI represent this error and option to retry.

### Developer Setup
```
- Just Run! 😎
```

## Developer Notes ##

### Design Pattern
- This project uses the **Model View View Model (MVVM)** pattern,

### Unit Testing
- The project uses XCTest for unit test. 

## Development Effort ##
This task duration is around 8 hrs listed as below:

- screen UI: 2hrs.
    - When Empty.
    - When data retrieved.
    - When loaded from cash.
    - When error happens.
- Network layer. 1 hr.
- Reading api documentation and handling it. 1 hr.
- Handling cash and its logic. 2 hrs.
- Refactoring and linking all together. 1 hr.
- Tests. 1 hr

 
