//
//  DataResponseDecoderMock.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/22/21.
//

import Foundation
@testable import Daily_Forecast

struct DataResponseDecoderMock: DataResponseDecoding {
    
    private let dataResponseDecoder: DataResponseDecoding
    var shouldError: Bool
    
    init(shouldError: Bool = false) {
        self.shouldError = shouldError
        self.dataResponseDecoder = DataResponseDecoder()
    }
    
    func decodeJson<F>(from feed: F, with data: Data) throws -> F.JSONResponseStructure where F : Feed {
        if shouldError{
            throw DataResponseDecodeError.decodeToJsonFailed
        }else{
            return try dataResponseDecoder.decodeJson(from: feed, with: data)
        }
    }
    
    func decodeModel<C>(from data: Data) throws -> C where C : Decodable {
        if shouldError{
            throw DecodingError.typeMismatch(
                PredictedWeather.self,
                DecodingError.Context(
                    codingPath: [],
                    debugDescription: "Error"
                )
            )
        }else{
            return try dataResponseDecoder.decodeModel(from: data)
        }
    }   
}
