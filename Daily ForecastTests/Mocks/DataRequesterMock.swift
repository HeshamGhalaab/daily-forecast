//
//  DataRequesterMock.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/22/21.
//

import Foundation
@testable import Daily_Forecast

struct DataRequesterMock: DataRequesting {
    
    private let urlSession: URLSessionRequesting
    let fetchError: FetchError?
    let shouldCacheData: Bool
    
    var response: [PredictedWeather]
    
    init(
        urlSession: URLSessionRequesting = URLSession(configuration: .default),
        response: [PredictedWeather] = [PredictedWeather(city: "Test City", date: "11/5/1993", temp: 12, icon: "test icon")],
        fetchError: FetchError?,
        shouldCacheData: Bool) {
        self.fetchError = fetchError
        self.shouldCacheData = shouldCacheData
        self.urlSession = urlSession
        self.response = response
    }
    
    func requestData<F>(from feed: F, completionHandler: @escaping (Result<Data, FetchError>) -> Void) where F : Feed {
        
        
        if let fetchError = fetchError{
            completionHandler(.failure(fetchError))
        }else{
           
            let data = try! JSONEncoder().encode(response)
            
            if shouldCacheData{
                guard let mockedRequest = urlRequest(with: feed.absolutePath, parameters: feed.parameters) else {return}
                
                let response = HTTPURLResponse(url: mockedRequest.url!, statusCode: 200, httpVersion: nil, headerFields: [:])!
                
                
                let cachedData = CachedURLResponse(response: response, data: data)
                self.urlSession.urlCache?.storeCachedResponse(cachedData, for: mockedRequest)
            }
            
            completionHandler(.success(data))
        }
        
    }
    
    private func urlRequest(with path: String, parameters: [String: String]?) -> URLRequest? {
        guard path.isValidUrl else { return nil }

        var urlComponents = URLComponents(string: path)

        let parameters = parameters?.map({ URLQueryItem(name: $0.key, value: $0.value)  })
        urlComponents?.queryItems = parameters
        if let url = urlComponents?.url {
            return URLRequest(url: url)
        }

        return nil
    }
    
    
    
    
}
