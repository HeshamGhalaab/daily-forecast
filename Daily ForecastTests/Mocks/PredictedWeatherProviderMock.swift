//
//  PredictedWeatherProviderMock.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/23/21.
//

import Foundation
@testable import Daily_Forecast

class PredictedWeatherProviderMock: PredictedWeatherProviding{
    
    var result: PredictedWeatherResult
    init(result: PredictedWeatherResult) {
        self.result = result
    }
    
    func fetchPredictedWeather(forCity city: String, completion: @escaping (PredictedWeatherResult) -> Void) {
        completion(result)
    }
}
