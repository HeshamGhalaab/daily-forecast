//
//  RequestManagerTests.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/23/21.
//

import XCTest
@testable import Daily_Forecast

class RequestManagerTests: XCTestCase {

    func testRequestManager_whenSucceed(){
        let mockedResponse = [
            PredictedWeather(city: "nasr", date: "12/2/2000", temp: 20, icon: "20"),
            PredictedWeather(city: "nasr", date: "12/3/2000", temp: 21, icon: "211")
        ]
        
        let dataRequesterMock: DataRequesting = DataRequesterMock(
            response: mockedResponse,
            fetchError: nil,
            shouldCacheData: false)
        
        let requestManager = RequestManager(dataRequester: dataRequesterMock)
        
        let feed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city":"nasr"])
        
        let completedExpectation = expectation(description: "completed")
        
        var requestResponse: [PredictedWeather] = []
        requestManager.request(from: feed) {
            switch $0{
            case .success(let response):
                requestResponse = response
                completedExpectation.fulfill()
            case .failure(_):
                XCTFail("Shouldn't have called")
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(requestResponse, mockedResponse)
    }
    
    func testRequestManager_whenFails(){
        
        let dataRequesterMock: DataRequesting = DataRequesterMock(
            fetchError: .fatal,
            shouldCacheData: false)
        
        let requestManager = RequestManager(dataRequester: dataRequesterMock)
        
        let feed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city":"nasr"])
        
        let completedExpectation = expectation(description: "fail")
        
        var fetchError: FetchError?
        requestManager.request(from: feed) {
            switch $0{
            case .success(_):
                XCTFail("Shouldn't have called")
            case .failure(let error):
                fetchError = error
                completedExpectation.fulfill()
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(fetchError, .fatal)
    }

}
