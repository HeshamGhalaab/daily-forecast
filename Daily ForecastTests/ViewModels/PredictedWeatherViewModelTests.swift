//
//  PredictedWeatherViewModelTests.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/23/21.
//

import XCTest
@testable import Daily_Forecast

class PredictedWeatherViewModelTests: XCTestCase {

    func testViewModel_whenFetchingSuccess(){
        let predictedWeatherMock = PredictedWeather(city: "", date: "", temp: 0, icon: "")
        let mockedResult = PredictedWeatherResult.new(values: [predictedWeatherMock])
        let providerMock: PredictedWeatherProviding!
        providerMock = PredictedWeatherProviderMock(result: mockedResult)
        
        let viewModel: PredictedWeatherViewModelProtocol!
        viewModel = PredictedWeatherViewModel(predictedWeatherProvider: providerMock)
        
        viewModel.outputs.didGetPredictedWeather = {
            XCTAssertEqual(viewModel.outputs.numberOfPredictedWeatherRows, 1)
            XCTAssertEqual(viewModel.outputs.predictedWeather(at: 0), predictedWeatherMock)
        }
        
        viewModel.outputs.didGetCachedPredictedWeather = {
            XCTFail("Shouldn't have called")
        }
        
        viewModel.outputs.fetchingPredictedWeatherDidFail = { _ in
            XCTFail("Shouldn't have called")
        }
    }
    
    func testViewModel_whenFetchingFail(){
        let mockedResult = PredictedWeatherResult.failure(error: "Error")
        let providerMock: PredictedWeatherProviding!
        providerMock = PredictedWeatherProviderMock(result: mockedResult)
        
        let viewModel: PredictedWeatherViewModelProtocol!
        viewModel = PredictedWeatherViewModel(predictedWeatherProvider: providerMock)
        
        viewModel.outputs.didGetPredictedWeather = {
            XCTFail("Shouldn't have called")
        }
        
        viewModel.outputs.didGetCachedPredictedWeather = {
            XCTFail("Shouldn't have called")
        }
        
        viewModel.outputs.fetchingPredictedWeatherDidFail = { error in
            XCTAssertEqual(viewModel.outputs.numberOfPredictedWeatherRows, 0)
            XCTAssertEqual("Error", error)
        }
    }
}
