//
//  PredictedWeatherCellViewModelTests.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/23/21.
//

import XCTest
@testable import Daily_Forecast

class PredictedWeatherCellViewModelTests: XCTestCase {

    func testViewModelOutputs(){
        let predictedWeather = PredictedWeather(city: "nasr city", date: "11/5/1993", temp: 12, icon: "icon")
        let viewModel: PredictedWeatherCellViewModelProtocol!
        viewModel = PredictedWeatherCellViewModel(predictedWeather: predictedWeather)
        XCTAssertEqual(viewModel.outputs.city, predictedWeather.city)
        XCTAssertEqual(viewModel.outputs.date, predictedWeather.date)
        XCTAssertEqual(viewModel.outputs.temp, predictedWeather.temp)
        XCTAssertEqual(viewModel.outputs.icon, predictedWeather.icon)
    }
}
