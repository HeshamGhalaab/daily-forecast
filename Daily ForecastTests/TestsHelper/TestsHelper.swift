//
//  TestsHelper.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/23/21.
//

import Foundation


class TestsHelper{
    
    enum Constant{
        static let SCHEME: String = "https"
        static let HOST: String = "daily-forecast.getsandbox.com"
        static let ALLOWED_DISK_SIZE: Int = 100 * 1024 * 1024
        static let MEMORY_CAPACTITY: Int = 0
        static let DISK_PATH: String = "predictedWeatherCacheTest"
    }
    
    static var forecastAbsolutePath: String{
        var components = URLComponents()
        components.scheme = Constant.SCHEME
        components.host = Constant.HOST
        components.path = "/forecast"
        return components.url?.absoluteURL.absoluteString ?? ""
    }
}
