//
//  CacheProviderTests.swift
//  Daily ForecastTests
//
//  Created by hesham ghalaab on 1/22/21.
//

import XCTest
@testable import Daily_Forecast

class CacheProviderTests: XCTestCase {

    var urlCache: URLCache = URLCache(
        memoryCapacity: TestsHelper.Constant.MEMORY_CAPACTITY,
        diskCapacity: TestsHelper.Constant.ALLOWED_DISK_SIZE,
        diskPath: TestsHelper.Constant.DISK_PATH)
    
    func testCacheProvider_IfURLCacheIsExistAfterInitializingIs() {
        let cacheProvider: CacheProviding = CacheProvider(cache: urlCache)
        XCTAssertEqual(cacheProvider.urlSession.urlCache, urlCache)
    }
    
    func testCacheProvider_whenDataExistsInCacheAfterRequestingIt(){
        
        let cacheProvider: CacheProviding = CacheProvider(cache: urlCache)
        
        let feed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city": "nasr city"])
        
        let mockedResponse =
            [
                PredictedWeather(city: "nasr city", date: "11/2/1900", temp: 10, icon: "1"),
                PredictedWeather(city: "nasr city", date: "11/3/1900", temp: 11, icon: "2")
            ]
        
        let dataRequesterMock: DataRequesting = DataRequesterMock(
            urlSession: cacheProvider.urlSession,
            response: mockedResponse,
            fetchError: nil,
            shouldCacheData: true)
        
        let requestManager = RequestManager(dataRequester: dataRequesterMock)
        requestManager.request(from: feed) {
            switch $0{
            case .success(_): break
            case .failure(_): XCTFail("Shouldn't have called")
            }
        }
        
        var responseFromCache = [PredictedWeather]()
        cacheProvider.checkCache(for: feed) { (result) in
            switch result{
            case .success(let response):
                responseFromCache = response
            case .failure(_):
                XCTFail("Shouldn't have called")
            }
        }
        
        XCTAssertEqual(responseFromCache, mockedResponse)
    }
    
    func testCacheProvider_whenRequestingAResponseThatIsNotInCache(){
        
        let cacheProvider: CacheProviding = CacheProvider(cache: urlCache)
        
        let feed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city": "Maadi"])
        
        let mockedResponse =
            [
                PredictedWeather(city: "Maadi", date: "12/2/2000", temp: 20, icon: "20"),
                PredictedWeather(city: "Maadi", date: "12/3/2000", temp: 21, icon: "211")
            ]
        
        let dataRequesterMock: DataRequesting = DataRequesterMock(
            urlSession: cacheProvider.urlSession,
            response: mockedResponse,
            fetchError: nil,
            shouldCacheData: true)
        
        let requestManager = RequestManager(dataRequester: dataRequesterMock)
        requestManager.request(from: feed) {
            switch $0{
            case .success(_): break
            case .failure(_): XCTFail("Shouldn't have called")
            }
        }
        
        let anotherFeed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city": "value shouldn't be in cache"])
        
        var fetchError: FetchError!
        cacheProvider.checkCache(for: anotherFeed) { (result) in
            switch result{
            case .success(_):
                XCTFail("Shouldn't have called")
            case .failure(let error):
                fetchError = error
            }
        }
        
        XCTAssertEqual(fetchError, .noContentReturned)
    }
    
    func testCacheProvider_whenFailedWithNonFatalError(){
        let cacheProvider: CacheProviding = CacheProvider(cache: urlCache)
        
        let feed = PredictedWeatherFeed(absolutePath: "", parameters: [:])
        
        var fetchError: FetchError!
        cacheProvider.checkCache(for: feed) { (result) in
            switch result{
            case .success(_):
                XCTFail("Shouldn't have called")
            case .failure(let error):
                fetchError = error
            }
        }
        
        XCTAssertEqual(fetchError, .nonFatal)
    }
    
    func testCacheProvider_whenFailedWithNoContentReturned(){
        let cacheProvider: CacheProviding = CacheProvider(cache: urlCache)
        
        let feed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city": "city that is not exist in cache"])
        
        var fetchError: FetchError!
        cacheProvider.checkCache(for: feed) { (result) in
            switch result{
            case .success(_):
                XCTFail("Shouldn't have called")
            case .failure(let error):
                fetchError = error
            }
        }
        
        XCTAssertEqual(fetchError, .noContentReturned)
    }
    
    func testCacheProvider_whenFailedWithDecodingError(){
        let responseDecoderMock = DataResponseDecoderMock(shouldError: true)
        
        let cacheProvider: CacheProviding = CacheProvider(
            cache: urlCache,
            responseDecoder: responseDecoderMock)
        
        let feed = PredictedWeatherFeed(
            absolutePath: TestsHelper.forecastAbsolutePath,
            parameters: ["city": "city in cache"])
        
        let dataRequesterMock = DataRequesterMock(
            urlSession: cacheProvider.urlSession,
            fetchError: nil,
            shouldCacheData: true)
        
        let requestManager = RequestManager(dataRequester: dataRequesterMock)
        requestManager.request(from: feed) {
            switch $0{
            case .success(_): break
            case .failure(_): XCTFail("Shouldn't have called")
            }
        }
        
        var fetchError: FetchError!
        cacheProvider.checkCache(for: feed) { (result) in
            switch result{
            case .success(_):
                XCTFail("Shouldn't have called")
            case .failure(let error):
                fetchError = error
            }
        }
        
        XCTAssertEqual(fetchError, .decodingError)
    }
}
