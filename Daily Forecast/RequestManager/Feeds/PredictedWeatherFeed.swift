//
//  PredictedWeatherFeed.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/22/21.
//

import Foundation

struct PredictedWeatherFeed: Feed {
    typealias JSONResponseStructure = [PredictedWeather]
    
    let absolutePath: String
    let parameters: [String: String]?
    
    init(absolutePath: String, parameters: [String: String]?) {
        self.absolutePath = absolutePath
        self.parameters = parameters
    }
}
