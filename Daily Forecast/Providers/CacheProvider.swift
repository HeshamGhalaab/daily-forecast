//
//  CacheProvider.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/22/21.
//

import Foundation
import os.log

protocol CacheProviding{
    var urlSession: URLSession {get}
    func checkCache<F: Feed>(for feed: F, completionHandler: @escaping (Result<F.JSONResponseStructure, FetchError>) -> Void)
}

class CacheProvider: CacheProviding{
    
    private let responseDecoder: DataResponseDecoding
    private let cache: URLCache
    
    init(cache: URLCache, responseDecoder: DataResponseDecoding = DataResponseDecoder()) {
        self.responseDecoder = responseDecoder
        self.cache = cache
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        sessionConfiguration.urlCache = cache
        urlSession = URLSession(configuration: sessionConfiguration)
    }
    
    var urlSession: URLSession
    
    func checkCache<F: Feed>(for feed: F, completionHandler: @escaping (Result<F.JSONResponseStructure, FetchError>) -> Void) {
        
        guard let request = self.urlRequest(with: feed.absolutePath, parameters: feed.parameters) else {
            return completionHandler(.failure(.nonFatal))
        }
        
        guard let cachedData = cache.cachedResponse(for: request) else {
            return completionHandler(.failure(.noContentReturned))
        }
        
        do {
            
            let model: F.JSONResponseStructure = try self.responseDecoder.decodeModel(from: cachedData.data)
            completionHandler(.success(model))
        } catch {
            completionHandler(.failure(.decodingError))
        }
    }
    
    private func urlRequest(with path: String, parameters: [String: String]?) -> URLRequest? {
        guard path.isValidUrl else { return nil }

        var urlComponents = URLComponents(string: path)

        let parameters = parameters?.map({ URLQueryItem(name: $0.key, value: $0.value)  })
        urlComponents?.queryItems = parameters
        if let url = urlComponents?.url {
            return URLRequest(url: url)
        }
        
        os_log("malformed url from path %@", log: .requestsLogger, type: .error, path)

        return nil
    }
}
