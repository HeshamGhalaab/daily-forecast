//
//  PredictedWeatherProvider.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/22/21.
//

import Foundation

private enum Constant{
    static let SCHEME: String = "https"
    static let HOST: String = "daily-forecast.getsandbox.com"
    static let ALLOWED_DISK_SIZE: Int = 100 * 1024 * 1024
    static let MEMORY_CAPACTITY: Int = 0
    static let DISK_PATH: String = "predictedWeatherCache"
}

enum PredictedWeatherResult {
    case failure(error: String)
    case new(values: [PredictedWeather])
    case cached(values: [PredictedWeather])
}

protocol PredictedWeatherProviding {
    func fetchPredictedWeather(forCity city: String, completion: @escaping (PredictedWeatherResult) -> Void)
}

class PredictedWeatherProvider: PredictedWeatherProviding{
    
    var url: URL? {
        var components = URLComponents()
        components.scheme = Constant.SCHEME
        components.host = Constant.HOST
        components.path = "/forecast"
        return components.url
    }
    
    func fetchPredictedWeather(forCity city: String, completion: @escaping (PredictedWeatherResult) -> Void) {
        
        let feed = createPredictedWeatherFeed(with: city)
        
        let cache: URLCache = URLCache(
            memoryCapacity: Constant.MEMORY_CAPACTITY,
            diskCapacity: Constant.ALLOWED_DISK_SIZE,
            diskPath: Constant.DISK_PATH)
        
        let cacheProvider: CacheProviding = CacheProvider(cache: cache)
        let dataRequester = DataRequester(urlSession: cacheProvider.urlSession)
        let requestManager = RequestManager(dataRequester: dataRequester)
        
        requestManager.request(from: feed) {
            switch $0{
            case .failure(let error):
                
                // If failure check cache
                cacheProvider.checkCache(for: feed) {
                    switch $0{
                    case .success(let response):
                        completion(.cached(values: response))
                    case .failure(_):
                        completion(.failure(error: error.errorDescription ?? ""))
                    }
                }
                
            case .success(let response):
                completion(.new(values: response))
            }
        }
    }
    
    func createPredictedWeatherFeed(with city: String) -> PredictedWeatherFeed{
        let absoluteURL = url?.absoluteURL.absoluteString ?? ""
        var parameters: [String: String] = [:]
        parameters["city"] = city
        return PredictedWeatherFeed(absolutePath: absoluteURL, parameters: parameters)
    }
}
