//
//  PredictedWeatherViewModel.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/18/21.
//

import Foundation
import os.log

protocol PredictedWeatherViewModelInputs {
    func fetchPredictedWeather(forCity city: String)
}

protocol PredictedWeatherViewModelOutputs {
    var fetchingPredictedWeatherDidFail: ( (_ error: String) -> Void)? { get set }
    var didGetCachedPredictedWeather: ( () -> Void)? { get set }
    var didGetPredictedWeather: ( () -> Void)? { get set }
    var numberOfPredictedWeatherRows: Int { get }
    var predictedWeatherResultTitle: String { get }
    
    func predictedWeather(at row: Int) -> PredictedWeather
}

protocol PredictedWeatherViewModelProtocol: AnyObject {
    var inputs: PredictedWeatherViewModelInputs { get }
    var outputs: PredictedWeatherViewModelOutputs { get set }
}

final class PredictedWeatherViewModel: PredictedWeatherViewModelInputs, PredictedWeatherViewModelOutputs, PredictedWeatherViewModelProtocol {
    
    var inputs: PredictedWeatherViewModelInputs { self }
    var outputs: PredictedWeatherViewModelOutputs {
        get { self }
        set { }
    }
    private var predictedWeatherProvider: PredictedWeatherProviding
    private var predictedWeather: [PredictedWeather]
    
    init(predictedWeatherProvider: PredictedWeatherProviding = PredictedWeatherProvider()) {
        self.predictedWeatherProvider = predictedWeatherProvider
        self.predictedWeather = []
        self.predictedWeatherResultTitle = String()
    }
    
    func fetchPredictedWeather(forCity city: String) {
        predictedWeatherProvider.fetchPredictedWeather(forCity: city) { (result) in
            switch result{
            case .failure(error: let error):
                self.predictedWeather = []
                self.predictedWeatherResultTitle = ""
                self.fetchingPredictedWeatherDidFail?(error)
            case .new(values: let values):
                self.predictedWeather = values
                self.predictedWeatherResultTitle = (values.count > 0) ? city: "No data found for \(city)"
                self.didGetPredictedWeather?()
            case .cached(values: let values):
                self.predictedWeather = values
                let title = (values.count > 0) ? "\(city), Not Accurate Data! ..": "No data found for \(city), Not Accurate Data! .."
                self.predictedWeatherResultTitle = title
                self.didGetCachedPredictedWeather?()
            }
        }
    }
    
    /// Outputs
    var fetchingPredictedWeatherDidFail: ((String) -> Void)?
    var didGetCachedPredictedWeather: (() -> Void)?
    var didGetPredictedWeather: (() -> Void)?
    
    var numberOfPredictedWeatherRows: Int {
        predictedWeather.count
    }
    
    var predictedWeatherResultTitle: String
    
    func predictedWeather(at row: Int) -> PredictedWeather{
        return predictedWeather[row]
    }
}

