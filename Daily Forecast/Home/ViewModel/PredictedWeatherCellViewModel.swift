//
//  PredictedWeatherCellViewModel.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/23/21.
//

import Foundation
import os.log

protocol PredictedWeatherCellViewModelInputs {
    
}

protocol PredictedWeatherCellViewModelOutputs {
    var city: String { get }
    var date: String { get }
    var icon: String { get }
    var temp: Int    { get }
}

protocol PredictedWeatherCellViewModelProtocol: AnyObject {
    var inputs: PredictedWeatherCellViewModelInputs { get }
    var outputs: PredictedWeatherCellViewModelOutputs { get }
}

final class PredictedWeatherCellViewModel: PredictedWeatherCellViewModelProtocol, PredictedWeatherCellViewModelInputs, PredictedWeatherCellViewModelOutputs {
    
    var inputs: PredictedWeatherCellViewModelInputs {self}
    var outputs: PredictedWeatherCellViewModelOutputs {self}
    
    private let predictedWeather: PredictedWeather
    
    init(predictedWeather: PredictedWeather) {
        self.predictedWeather = predictedWeather
    }
    
    // Outputs
    var city: String { predictedWeather.city }
    var date: String { predictedWeather.date }
    var icon: String { predictedWeather.icon }
    var temp: Int    { predictedWeather.temp }
}
