//
//  PredictedWeather.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/18/21.
//

import Foundation

// MARK: - predictedWeather
struct PredictedWeather: Codable, Equatable{
    let city: String
    let date: String
    let temp: Int
    let icon: String
}
