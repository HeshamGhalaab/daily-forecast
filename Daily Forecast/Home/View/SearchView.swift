//
//  SearchView.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/23/21.
//

import UIKit

class SearchView: UIView {

    @IBOutlet private var contentView: UIView!
    @IBOutlet private weak var searchField: UITextField!
    
    private let IDENTIFIER = "SearchView"
    var searchByCity: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(IDENTIFIER, owner: self, options: nil)
        contentView.fixInView(self)
        searchField.delegate = self
        setupSearchIcon()
    }
    
    private func setupSearchIcon(){
        let button = UIButton(type: .custom)
        button.setImage(
            UIImage(systemName: "magnifyingglass"),
            for: .normal)
        
        button.imageEdgeInsets = UIEdgeInsets(
            top: 0, left: -16,
            bottom: 0, right: 0)
        
        button.frame = CGRect(
            x: CGFloat(searchField.frame.size.width - 25),
            y: CGFloat(5),
            width: CGFloat(25),
            height: CGFloat(25))
        
        button.addTarget(
            self,
            action: #selector(self.callSearchByCity),
            for: .touchUpInside)
        searchField.rightView = button
        searchField.rightViewMode = .always
    }
    
    @objc private func callSearchByCity(){
        self.endEditing(true) // Hide keyBoard
        self.searchByCity?()
    }
    
    var searchText: String?{
        searchField.text
    }
    
}

extension SearchView: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.callSearchByCity()
        return true
    }
}
