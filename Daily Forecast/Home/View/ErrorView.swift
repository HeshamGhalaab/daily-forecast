//
//  ErrorView.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/23/21.
//

import UIKit

class ErrorView: UIView {

    @IBOutlet private var contentView: UIView!
    @IBOutlet private weak var errorLabel: UILabel!
    
    private let IDENTIFIER = "ErrorView"
    var retryTapped: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(IDENTIFIER, owner: self, options: nil)
        contentView.fixInView(self)
    }
    
    @IBAction private func onTapRetry(_ sender: UIButton) {
        retryTapped?()
    }
    
    func set(error: String){
        self.errorLabel.text = error
    }
    
}
