//
//  PredictedWeatherCell.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/23/21.
//

import UIKit

class PredictedWeatherCell: UITableViewCell {

    @IBOutlet weak private var dayLabel: UILabel!
    @IBOutlet weak private var temperatureLabel: UILabel!
    @IBOutlet weak private var temperatureIcon: UIImageView!
    
    func configure(with viewModel: PredictedWeatherCellViewModelProtocol){
        dayLabel.text = viewModel.outputs.date
        temperatureLabel.text = viewModel.outputs.temp.description + "º"
        temperatureIcon.image = UIImage(named: viewModel.outputs.icon)
    }
}
