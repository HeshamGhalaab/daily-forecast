//
//  PredictedWeatherViewController.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/18/21.
//

import UIKit

class PredictedWeatherViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: SearchView!
    @IBOutlet weak var errorView: ErrorView!
    
    // MARK: Properties
    var viewModel: PredictedWeatherViewModelProtocol!
    var weatherTableViewDataSource: WeatherTableViewDataSource!
    
    // MARK: Override Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
    }
    
    private func configuration(){
        viewModelConfiguration()
        tableViewConfiguration()
        searchViewConfiguration()
        errorViewConfiguration()
    }
    
    private func tableViewConfiguration(){
        let nib = UINib(nibName: PredictedWeatherCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: PredictedWeatherCell.identifier)
        
        weatherTableViewDataSource = WeatherTableViewDataSource(viewModel: self.viewModel)
        tableView.dataSource = weatherTableViewDataSource
        tableView.tableFooterView = UIView()
    }
    
    private func viewModelConfiguration(){
        viewModel = PredictedWeatherViewModel()
        bindViewModel()
    }
    
    func bindViewModel(){
        viewModel.outputs.didGetPredictedWeather = { [weak self] in
            guard let self = self else {return}
            self.tableView.reloadData()
            self.setErrorView(with: nil)
        }
        
        viewModel.outputs.didGetCachedPredictedWeather = { [weak self] in
            guard let self = self else {return}
            self.tableView.reloadData()
            self.setErrorView(with: nil)
        }
        
        viewModel.outputs.fetchingPredictedWeatherDidFail = { [weak self] error in
            guard let self = self else {return}
            self.tableView.reloadData()
            self.setErrorView(with: error)
        }
    }
    
    private func searchViewConfiguration(){
        searchView.searchByCity = { [weak self] in
            guard let self = self else {return}
            self.searchByCity()
        }
    }
    
    private func errorViewConfiguration(){
        errorView.retryTapped = { [weak self] in
            guard let self = self else {return}
            self.searchByCity()
        }
    }
    
    private func setErrorView(with error: String?){
        if let error = error{
            errorView.set(error: error)
            errorView.isHidden = false
        }else{
            errorView.isHidden = true
        }
    }
    
    @objc func searchByCity(){
        guard let text = searchView.searchText, !text.isEmpty else {
            self.alert(title: "Search field is empty!", message: "Please enter city name.", handler: {})
            return
        }
        viewModel.inputs.fetchPredictedWeather(forCity: text)
    }
}
