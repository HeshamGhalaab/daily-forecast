//
//  WeatherTableViewDataSource.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/23/21.
//

import UIKit

class WeatherTableViewDataSource: NSObject, UITableViewDataSource {
    
    var viewModel: PredictedWeatherViewModelProtocol!
    
    init(viewModel: PredictedWeatherViewModelProtocol) {
        self.viewModel = viewModel
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.outputs.numberOfPredictedWeatherRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: PredictedWeatherCell.identifier,
            for: indexPath) as! PredictedWeatherCell
        
        
        let cellViewModel: PredictedWeatherCellViewModelProtocol!
        cellViewModel = PredictedWeatherCellViewModel(
            predictedWeather: viewModel.outputs.predictedWeather(at: indexPath.row))
        cell.configure(with: cellViewModel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.outputs.predictedWeatherResultTitle
    }
}
