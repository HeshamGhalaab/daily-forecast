//
//  StoryboardMakeable.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/17/21.
//

import UIKit

protocol StoryboardMakeable: AnyObject {

    associatedtype StoryboardMakeableType
    static var storyboardName: String { get }
    static func make() -> StoryboardMakeableType
}

extension StoryboardMakeable where Self: UIViewController {

    static func make() -> StoryboardMakeableType {
        let viewControllerId = String(describing: self)

        return make(with: viewControllerId)
    }

    static func make(with viewControllerId: String) -> StoryboardMakeableType {
        let storyboard = UIStoryboard(name: storyboardName,
                                      bundle: Bundle(for: self))

        guard let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId) as? StoryboardMakeableType else {
            fatalError("Did not find \(viewControllerId) in \(storyboardName)!")
        }

        return viewController
    }
}

