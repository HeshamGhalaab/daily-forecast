//
//  CellIdentifier.swift
//  Daily Forecast
//
//  Created by hesham ghalaab on 1/17/21.
//

import Foundation
import UIKit

extension UITableViewCell {
    static var identifier: String {
         String(describing: self)
    }
}

